'use strict';

var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var FrontmendGenerator = yeoman.generators.Base.extend({

  init: function () {

    this.pkg = require('../package.json');

    this.gruntPlugins = [];

    this.on('end', function () {

      this.installDependencies({
        skipInstall: this.options['skip-install'],
        callback: function () {
          this.spawnCommand('npm', ['prune']);
          this.spawnCommand('bower', ['prune']);
        }.bind(this)
      });

    });

  },

  askFor: function () {

    var done = this.async();

    // have Yeoman greet the user
    this.log(this.yeoman);

    this.log(chalk.magenta('You\'re using the Frontmend generator.'));

    var prompts = [
      {
        type: 'input',
        name: 'projectName',
        message: 'What\'s the name of this project?',
        default: 'Project Name'
      },
      {
        type: 'confirm',
        name: 'includeExpressJS',
        message: 'Will this be an ExpressJS project?',
        default: false
      },
      {
        type: 'checkbox',
        name: 'bowerPackages',
        message: 'Which of these bower packages would you like to install?',
        choices: [
          {
            name: 'RequireJS',
            value: 'requirejs',
            checked: true
          },
          {
            name: 'Almond',
            value: 'almond',
            checked: true
          },
          {
            name: 'jQuery',
            value: 'jquery',
            checked: true
          },
          {
            name: 'Backbone',
            value: 'backbone',
            checked: false
          },
          {
            name: 'Lodash',
            value: 'lodash',
            checked: false
          }
        ]
      },
      {
        type: 'list',
        name: 'templateEngine',
        message: 'Which JavaScript templating engine will you be using?',
        choices: [
          {
            name: 'None',
            value: false
          },
          {
            name: 'Handlebars',
            value: 'handlebars'
          },
          {
            name: 'Mustache',
            value: 'mustache.js'
          },
          {
            name: 'Other',
            value: 'other'
          }
        ],
        default: 0
      },
      {
        type: 'list',
        name: 'preprocessor',
        message: 'Which CSS preprocessor will you be using?',
        choices: [
          {
            name: 'Stylus',
            value: 'stylus'
          },
          {
            name: 'SASS',
            value: 'sass'
          },
          {
            name: 'Less',
            value: 'less'
          },
          {
            name: 'None',
            value: false
          }
        ],
        default: 0
      },
      {
        type: 'confirm',
        name: 'modernizr',
        message: 'Will you be including Modernizr in this project?',
        default: false
      }
    ];

    this.prompt(prompts, function (props) {

      this.projectName = props.projectName;
      this.includeExpressJS = props.includeExpressJS;
      this.bowerPackages = props.bowerPackages;
      this.templateEngine = props.templateEngine;
      this.modernizr = props.modernizr;
      this.preprocessor = props.preprocessor;

      console.log('Props: ', props);

      done();

    }.bind(this));

  },

  app: function () {

    var dir = 'src';

    if (this.includeExpressJS) {

      this.mkdir('app');

      // TO DO: add express related file structure here
      // TO DO: all express related stuff should probably be a sub generator

      dir = 'public/src';

    }

    this.mkdir(dir + '/js');

    if (this.modernizr === true) {

      this.bowerPackages.push('modernizr');
      this.gruntPlugins.push('grunt-modernizr');

    }

    if (this.preprocessor !== false) {

      this.gruntPlugins.push('grunt-contrib-' + this.preprocessor);
      this.directory(this.preprocessor, dir + '/' + this.preprocessor);

    } else {

      this.mkdir(dir + '/css');
      this.write(dir + '/css/main.css', '');

    }

    if (this.templateEngine !== false) {

      if (this.templateEngine !== 'other') {

        this.bowerPackages.push(this.templateEngine);
        this.gruntPlugins.push('grunt-contrib-' + this.templateEngine);

      }

      this.mkdir(dir + '/templates');

    }

    this.template('_bower.json', 'bower.json');
    this.template('_Gruntfile.js', 'Gruntfile.js');
    this.template('_package.json', 'package.json');
    this.template('_README.md', 'README.md');

  },

  projectfiles: function () {

    this.copy('bowerrc', '.bowerrc');
    this.copy('editorconfig', '.editorconfig');
    this.copy('gitattributes', '.gitattributes');
    this.copy('gitignore', '.gitignore');
    this.copy('jshintignore', '.jshintignore');
    this.copy('jshintrc', '.jshintrc');

  }

});

module.exports = FrontmendGenerator;