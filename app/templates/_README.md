# <%= projectName %>

## Summary

Add a summary here...

## System Dependencies

- [Node.js](http://nodejs.org/download/)
    - [Yo](http://yeoman.io) – `npm install -g yo`

## Local Development

Add local development instructions here...

1. Install system dependencies (see above).
2. Install project dependencies by running `npm install && bower install` in the project root.
3. Run the default grunt task `grunt`.

### Grunt Tasks

Add documentation of grunt tasks here...

### Node Environments

The node environment can be set by prefixing the `node` command with `NODE_ENV=ENVIRONMENT` on Mac OSX and `set NODE_ENV=ENVIRONMENT` on Windows. E.g.

- `NODE_ENV=development node app`
- `NODE_ENV=testing node app`
- `NODE_ENV=production node app`

## Authors

- [Saul](http://mailto:saul@allofus.com)